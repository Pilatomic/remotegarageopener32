#include "board.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"


#define GPIO_LED            25
#define GPIO_VEXT_ENA       21
#define GPIO_BUTTON         0

#define GPIO_OLED_RST       16

#define GPIO_SPI_MISO       19
#define GPIO_SPI_MOSI       27
#define GPIO_SPI_CLK        5

#define GPIO_SX1276_RST     14
#define GPIO_SX1276_NSS     18
#define GPIO_SX1276_DIO     12

#define GPIO_OUTPUT_PIN_SEL     ((1ULL<<GPIO_LED) | (1ULL<<GPIO_VEXT_ENA) | (1ULL<<GPIO_SX1276_RST) | (1ULL<<GPIO_OLED_RST))
#define GPIO_INPUT_PIN_SEL      (1ULL<<GPIO_BUTTON)

static ButtonPressed buttonCb = NULL;

static void buttonIsrHandler(void* v)
{
    if(buttonCb) buttonCb();
}

void board_init()
{

    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    ESP_ERROR_CHECK(gpio_config(&io_conf));

    board_setLed(false);
    board_enableVext(false);
    board_setOledRst(false);
    board_setSX1276Rst(false);

    io_conf.intr_type = GPIO_INTR_NEGEDGE;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 1;
    ESP_ERROR_CHECK(gpio_config(&io_conf));

    ESP_ERROR_CHECK(gpio_install_isr_service(0));
    ESP_ERROR_CHECK(gpio_isr_handler_add(GPIO_BUTTON, &buttonIsrHandler, NULL));
}


void board_setButtonPressedCb(ButtonPressed cb)
{
    buttonCb = cb;
}

void board_setLed(uint32_t b)
{
    gpio_set_level(GPIO_LED, b);
}

void board_enableVext(uint32_t b)
{
    gpio_set_level(GPIO_VEXT_ENA, !b);
}

void board_setSX1276Rst(uint32_t b)
{
    gpio_set_level(GPIO_SX1276_RST, !b);
}

void board_setOledRst(bool b)
{
    gpio_set_level(GPIO_OLED_RST, !b);
}

static void flashLedTask(void* p)
{
    uint32_t d = (uint32_t) p;
    board_setLed(true);
    vTaskDelay(d / portTICK_PERIOD_MS);
    board_setLed(false);
    vTaskDelete(NULL);
}

void board_flashLed(uint32_t durationMs)
{
    xTaskCreate(flashLedTask, "flashled task", configMINIMAL_STACK_SIZE, (void*)durationMs, 1, NULL);
}

void board_setSpiIoNums(spi_bus_config_t *spiCfg)
{
    spiCfg->miso_io_num = GPIO_SPI_MISO;
    spiCfg->mosi_io_num = GPIO_SPI_MOSI;
    spiCfg->sclk_io_num = GPIO_SPI_CLK;
}

uint32_t board_getSX1276NSSpin()
{
    return GPIO_SX1276_NSS;
}

uint32_t board_getSX1276DIOpin()
{
    return GPIO_SX1276_DIO;
}

