#ifndef BOARD_H
#define BOARD_H
#include <stdint.h>
#include "driver/spi_master.h"

void board_init();
void board_enableVext(uint32_t b);

typedef void (*ButtonPressed)(void);

void board_setButtonPressedCb(ButtonPressed cb);

void board_setLed(uint32_t b);
void board_flashLed(uint32_t durationMs);

void board_setOledRst(bool b);

void board_setSX1276Rst(uint32_t b);
uint32_t board_getSX1276NSSpin();
uint32_t board_getSX1276DIOpin();
void board_setSpiIoNums(spi_bus_config_t* spiCfg);

#endif
