#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include <stdio.h>
#include "wifiscan.h"
#include "ssd1306.h"
#include "remote.h"
#include "board.h"

char lcdText[256] = "Scanning ...";


void onScanDone(uint8_t foundApCount)
{
    uint16_t apCount = foundApCount;

    wifi_ap_record_t *list = (wifi_ap_record_t *) malloc(sizeof(wifi_ap_record_t) *apCount);
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&apCount, list));


    //Display found SSID on OLED screen
    int dispSsidCount = 7;
    if(apCount < dispSsidCount) dispSsidCount = apCount;

    int i = sprintf(lcdText, "Found %d AP", apCount);
    for(int c = 0 ; c < dispSsidCount ; c++)
    {
        i += sprintf(lcdText+i, "\n%.16s", (char *)list[c].ssid );
    }
    xTaskCreate(task_displayText, "text task", 2048, lcdText, 10, NULL);

    //Print found AP
    printf("\n");
    printf("     MAC addr     |                SSID              | Channel | RSSI\n");
    printf("---------------------------------------------------------------------\n");
    for(int i = 0; i < apCount; i++)
    {
        printf("%02x:%02x:%02x:%02x:%02x:%02x | %32s | %7d | %4d\n",
                list[i].bssid[0],list[i].bssid[1],list[i].bssid[2],list[i].bssid[3],list[i].bssid[4],list[i].bssid[5],
                (char *)list[i].ssid, list[i].primary, list[i].rssi);
    }

    printf("---------------------------------------------------------------------\n");

    free(list);
}


void onButtonPressed()
{
    board_flashLed(500);
    remote_transmit();
}

int app_main(void)
{
    nvs_flash_init();

    board_init();
    board_enableVext(true);
    board_setButtonPressedCb(&onButtonPressed);

    i2c_master_init();
    ssd1306_init();

    remote_init();

    wifiscan_init();
    wifiscan_setScanDoneCb(&onScanDone);
    wifiscan_startScanning();

    xTaskCreate(task_displayText, "text task", 2048, lcdText, 10, NULL);

    return 0;
}
