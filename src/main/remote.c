#include "remote.h"
#include "sx1276.h"
#include "board.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "driver/rmt.h"

#define RMT_CHANNEL 0
#define RMT_CLK_DIV 100

#define CODE_BASE_LENGTH    560

const char* TAG = "REMOTE";


/*static const uint8_t codeItems[] =
{
    2,
    0, 1, 0, 0, 1, 1, 1, 0,
    1, 0, 1, 1, 0, 0, 0, 0,
    1, 1, 1, 1, 0, 0, 0, 1,
    1, 1, 0, 0, 1, 0, 0, 0,
    1, 0, 0, 0, 1, 1, 0, 1,
    1, 0, 1, 1, 0, 1, 0, 1,
};*/

static const uint8_t codeItems[] =
{
    2,
    1, 1, 1, 0, 1, 1, 1, 1,
    1, 0, 0, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 1, 0, 0, 1,
    1, 1, 0, 0, 1, 0, 0, 0,
    1, 0, 0, 0, 1, 1, 0, 1,
    1, 1, 0, 0, 0, 0, 1, 0,
};


static const rmt_item32_t itemsTypes[] =
{
    {
        .duration0 = CODE_BASE_LENGTH * 2,
        .level0 = 0,
        .duration1 = CODE_BASE_LENGTH * 1,
        .level1 = 1
    },
    {
        .duration0 = CODE_BASE_LENGTH * 1,
        .level0 = 0,
        .duration1 = CODE_BASE_LENGTH * 2,
        .level1 = 1
    },
    {
        .duration0 = CODE_BASE_LENGTH * 1,
        .level0 = 1,
        .duration1 = CODE_BASE_LENGTH * 2,
        .level1 = 1
    }
};

static void fillRmtBlock()
{
    for(uint16_t i = 0 ; i < (sizeof(codeItems) / sizeof(codeItems[0])) ; i++)
    {
        uint8_t type = codeItems[i];
        ESP_ERROR_CHECK(rmt_fill_tx_items(RMT_CHANNEL, &itemsTypes[type], 1, i));
    }
    //Empty item at the end
    rmt_item32_t endItem = {.val= 0ULL};
    ESP_ERROR_CHECK(rmt_fill_tx_items(RMT_CHANNEL, &endItem, 1, (sizeof(codeItems) / sizeof(codeItems[0]))));
}

static TaskHandle_t txTaskHandle = NULL;

static void txendcb(rmt_channel_t c, void* arg)
{
    xTaskNotifyGive(txTaskHandle);
}


static void task_transmit(void* v)
{
    uint32_t count = *(uint32_t*)v;
    board_setLed(true);
    sx1276_tx();
    vTaskDelay(50 / portTICK_PERIOD_MS);

    while(count > 0)
    {
        rmt_tx_start(RMT_CHANNEL, true);
        if(0 == ulTaskNotifyTake(pdTRUE, 1000 / portTICK_PERIOD_MS))
        {
            ESP_LOGE(TAG, "tx wait timeout");
            break;
        }
        count--;

        vTaskDelay(20 / portTICK_PERIOD_MS);
    }

    sx1276_idle();
    board_setLed(false);

    ESP_LOGI(TAG, "tx completed");

    txTaskHandle = NULL;
    vTaskDelete(NULL);
}

int32_t remote_init()
{
    if(sx1276_init() != 0) return -1;

    //Setup rmt
    rmt_config_t rmt_tx;
    rmt_tx.channel = RMT_CHANNEL;
    rmt_tx.gpio_num = board_getSX1276DIOpin();
    rmt_tx.mem_block_num = 1;
    rmt_tx.clk_div = RMT_CLK_DIV;
    rmt_tx.tx_config.loop_en = false;
    rmt_tx.tx_config.carrier_en = false;
    rmt_tx.tx_config.idle_level = RMT_IDLE_LEVEL_LOW;
    rmt_tx.tx_config.idle_output_en = true;
    rmt_tx.rmt_mode = RMT_MODE_TX;
    ESP_ERROR_CHECK(rmt_config(&rmt_tx));

    ESP_ERROR_CHECK(rmt_driver_install(RMT_CHANNEL, 0, ESP_INTR_FLAG_IRAM));
    rmt_register_tx_end_callback(txendcb, NULL);

    fillRmtBlock();

    ESP_LOGI(TAG, "Init done");

    return 0;
}


void remote_transmit()
{

    uint32_t txCount = 20;
    if(txTaskHandle == NULL ) xTaskCreate(task_transmit, "remote tx task", 4096, &txCount, 10, &txTaskHandle);

}
