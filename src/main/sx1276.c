#include "sx1276.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "driver/spi_master.h"
#include "board.h"



#define REG_FIFO                        0x00
#define REG_OP_MODE                     0x01
#define REG_FRF_MSB                     0x06
#define REG_FRF_MID                     0x07
#define REG_FRF_LSB                     0x08
#define REG_PA_CONFIG                   0x09
#define REG_LNA                         0x0c
#define REG_PACKET_CONFIG_2             0x31
#define REG_IRQ_FLAGS_1                 0x3e
#define REG_IRQ_FLAGS_2                 0x3f
#define REG_VERSION                     0x42

#define VALUE_VERSION                   0x12
#define VALUE_FSTEP                     61

#define MODE_MOD_OOK                    0x20
#define MODE_LOW_FREQ                   0x08
#define MODE_SLEEP                      0x00
#define MODE_STDBY                      0x01
#define MODE_FSTX                       0x02
#define MODE_TX                         0x03
#define MODE_FSRX                       0x04
#define MODE_RX                         0x05

#define MODE_MODULATION                 (MODE_MOD_OOK | MODE_LOW_FREQ)

static const char* TAG = "SX1276";
static spi_device_handle_t spi = NULL;

enum Mode {
    Mode_Sleep = MODE_SLEEP,
    Mode_Stdby = MODE_STDBY,
    Mode_Tx = MODE_TX,
    Mode_Rx = MODE_RX
};

static void sx1276_writeReg (uint8_t addr, uint8_t data ) {
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.flags = SPI_TRANS_USE_TXDATA;
    t.length=8;
    t.cmd = addr | 0x80;
    t.tx_data[0] = data;
    ESP_ERROR_CHECK(spi_device_polling_transmit(spi, &t));  //Transmit!
}

static uint8_t sx1276_readReg (uint8_t addr) {
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.flags = SPI_TRANS_USE_RXDATA;
    t.rxlength=8;
    t.cmd = addr & 0x7F;
    ESP_ERROR_CHECK(spi_device_polling_transmit(spi, &t));  //Transmit!
    return t.rx_data[0];
}

static void sx1276_reset(void)
{
    board_setSX1276Rst(true);
    vTaskDelay(10 / portTICK_RATE_MS);
    board_setSX1276Rst(false);
    vTaskDelay(10 / portTICK_RATE_MS);
}

static void sx1276_setMode(enum Mode m)
{
    sx1276_writeReg(REG_OP_MODE, MODE_MODULATION | (uint8_t)m );
}

static void sx1276_setCarrierHz(uint32_t freqHz)
{
    uint32_t frf = freqHz / VALUE_FSTEP;
    sx1276_writeReg(REG_FRF_MSB, (uint8_t) ((frf >> 16) & 0xFF));
    sx1276_writeReg(REG_FRF_MID, (uint8_t) ((frf >> 8) & 0xFF));
    sx1276_writeReg(REG_FRF_LSB, (uint8_t) (frf & 0xFF));
}

static void sx1276_dumpStatus()
{
    uint32_t f2 = sx1276_readReg(REG_FRF_MSB);
    uint32_t f1 = sx1276_readReg(REG_FRF_MID);
    uint32_t f0 = sx1276_readReg(REG_FRF_LSB);
    uint8_t status1 = sx1276_readReg(REG_IRQ_FLAGS_1);
    uint8_t status2 = sx1276_readReg(REG_IRQ_FLAGS_2);
    uint32_t freq = ((f2 << 16) | (f1 << 8) | f0) * VALUE_FSTEP;

    ESP_LOGI(TAG, "freq=%d Hz, IrqFlags1=0x%.2X, IrqFlags2=0x%.2X", freq, status1, status2);
}

int32_t sx1276_init()
{
    // Spi Hardware Init
    spi_bus_config_t buscfg={
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz=8
    };

    board_setSpiIoNums(&buscfg);

    spi_device_interface_config_t devcfg={
        .clock_speed_hz=1000000,                //Clock out at 1 MHz
        .mode=0,                                //SPI mode 0
        .spics_io_num=board_getSX1276NSSpin(),  //CS pin
        .queue_size=1,                          //We want to be able to queue 7 transactions at a time
        .command_bits = 8,
        .flags = SPI_DEVICE_HALFDUPLEX,
        .cs_ena_pretrans = 16,
        .cs_ena_posttrans = 16,
    };
    ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &buscfg, 0));
    ESP_ERROR_CHECK(spi_bus_add_device(HSPI_HOST, &devcfg, &spi));

    //SX1276 Init
    sx1276_reset();

    sx1276_setMode(Mode_Sleep);

    uint8_t readVersion = sx1276_readReg(REG_VERSION);
    if(readVersion != VALUE_VERSION)
    {
        ESP_LOGE(TAG, "version mismatch (0x%.2x instead of 0x%.2x) : SPI error ?",
                 readVersion, VALUE_VERSION);
        return -1;
    }

    sx1276_setCarrierHz(433891000);
    sx1276_writeReg(REG_PACKET_CONFIG_2, 0x00); //Continous Mode
    sx1276_setMode(Mode_Stdby);
   // sx1276_setMode(Mode_Tx);

    sx1276_dumpStatus();
    return 0;
}

void sx1276_idle()
{
    sx1276_setMode(Mode_Stdby);
}

void sx1276_tx()
{
    sx1276_setMode(Mode_Tx);
}

void sx1276_rx()
{
    sx1276_setMode(Mode_Rx);
}
