#ifndef SX1276_H
#define SX1276_H

#include <stdint.h>

int32_t sx1276_init();
void sx1276_idle();
void sx1276_tx();
void sx1276_rx();

#endif
