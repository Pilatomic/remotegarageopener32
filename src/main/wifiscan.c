#include "wifiscan.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

bool continueScanning = false;
WifiScanDoneCB scanDoneCb = NULL;
const char logTag[] = "WIFISCAN";

void initiateScan()
{
    //let us test a wifi scan ...
    wifi_scan_config_t scanConf = {
        .ssid = NULL,
        .bssid = NULL,
        .channel = 0,
        .show_hidden = 1,
        .scan_type = WIFI_SCAN_TYPE_PASSIVE,
        .scan_time.passive = 5000 / 12
    };

    //ESP_LOGI(TAG, "start scan");
    ESP_ERROR_CHECK(esp_wifi_scan_start(&scanConf, 0));
}

void event_handler(void* arg, esp_event_base_t event_base,
                   int32_t event_id, void* event_data)
{
    wifi_event_sta_scan_done_t* scanDoneEvent = (wifi_event_sta_scan_done_t*) event_data;
    uint8_t apCount = scanDoneEvent->number;
    ESP_LOGI(logTag, "WiFi Scan Completed, found %d AP", apCount);
    if(scanDoneCb) scanDoneCb(apCount);
    if(continueScanning) initiateScan();
}

void wifiscan_init()
{
    tcpip_adapter_init();

    //Initialize the system event handler
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_SCAN_DONE, &event_handler, NULL));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_start());

    /*//Station mode config
                wifi_config_t stacfg = {
                    .sta = {
                        .ssid = "",
                        .password = "",
                        .scan_method = WIFI_ALL_CHANNEL_SCAN,
                        .sort_method = WIFI_CONNECT_AP_BY_SIGNAL,
                        .threshold.rssi = -127,
                        .threshold.authmode = WIFI_AUTH_OPEN,
                    },
                };
                ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_MODE_STA, &stacfg))*/;

}

void wifiscan_startScanning()
{
    continueScanning = true;
    initiateScan();
}

void wifiscan_stopScanning()
{
    continueScanning = false;
}

void wifiscan_setScanDoneCb(WifiScanDoneCB cb)
{
    scanDoneCb = cb;
}
