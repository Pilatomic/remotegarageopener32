#ifndef WIFISCAN_H
#define WIFISCAN_H
#include <stdint.h>

typedef void (*WifiScanDoneCB)(uint8_t apCount);

void wifiscan_init();
void wifiscan_setScanDoneCb(WifiScanDoneCB cb);
void wifiscan_startScanning();
void wifiscan_stopScanning();


#endif
